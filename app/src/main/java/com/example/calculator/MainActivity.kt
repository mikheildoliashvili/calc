package com.example.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import kotlin.math.pow
import kotlin.math.sqrt

class MainActivity : AppCompatActivity() {

    private lateinit var TextViewResult: TextView

    private var operand: Double = 0.0
    private var operation: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        TextViewResult = findViewById(R.id.TextViewResult)

    }

    fun numberClick(clickedView: View) {

        if (clickedView is TextView) {

            var result = TextViewResult.text.toString()
            val number = clickedView.text.toString()

            if(result == "0"){
                result = ""
            }

            TextViewResult.text = result + number

        }

    }

    fun operationClick (clickedView: View) {

        if(clickedView is TextView) {

            val result = TextViewResult.text.toString()

            if(result.isNotEmpty()) {
                this.operand = result.toDouble()
            }

            this.operation = clickedView.text.toString()

            TextViewResult.text = ""

        }

    }
    fun clearClick(clickedView: View){

        if (clickedView is TextView){

            var result: String = TextViewResult.text.toString()
            if (result.isNotEmpty()){
                result = "0"
            }
            TextViewResult.text = result
        }
    }

    fun delClick(clickedView: View){

        if (clickedView is TextView){

            val result = TextViewResult.text.toString()
            if (result.isNotEmpty()){

                TextViewResult.text = result.dropLast(1)

            }
        }
    }

    fun equalsClick(clickedView: View) {
        if(clickedView is TextView){
            val secondOperandText = TextViewResult.text.toString()
            var secondOperand :Double = 0.0

        if (secondOperandText.isNotEmpty()){
            secondOperand=secondOperandText.toDouble()
        }
            when(operation){
                "+"->TextViewResult.text=(operand+secondOperand).toString()
                "*"->TextViewResult.text=(operand*secondOperand).toString()
                "-"->TextViewResult.text=(operand-secondOperand).toString()
                "/"->TextViewResult.text=(operand/secondOperand).toString()
                "^"->TextViewResult.text=(operand.pow(secondOperand)).toString()
                "√"->TextViewResult.text= sqrt(operand).toString()
                "DEL"-> TextViewResult.text = TextViewResult.text.drop(1)
                "CLEAR"-> TextViewResult.text = ""

            }


        }
    }

}